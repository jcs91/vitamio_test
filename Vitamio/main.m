//
//  main.m
//  Vitamio
//
//  Created by Joyal Serrao on 15/01/17.
//  Copyright © 2017 Joyal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
