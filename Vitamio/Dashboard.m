//
//  Dashboard.m
//  Vitamio
//
//  Created by Joyal Serrao on 16/01/17.
//  Copyright © 2017 Joyal. All rights reserved.
//

#import "Dashboard.h"
#import "SlideNavigationController.h"
@interface Dashboard ()<UICollectionViewDelegate,UICollectionViewDataSource>
{
    NSArray *menuList;
}
@end

@implementation Dashboard

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title=@"Home";
    // Do any additional setup after loading the view.
     menuList=[[NSArray alloc]initWithObjects:@"Live TV",@"Masjid",@"On Demand",@"Azan",@"Phone Pluse",@"Money Transfer",nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark collection date source and  delegate

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return menuList.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier = @"dashboardCell";
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    collectionView.allowsMultipleSelection = YES;
    
    UIView *dashBackground=(UIView *)[cell viewWithTag:201];
     UILabel *dashName=(UILabel *)[cell viewWithTag:202];
    UIImageView *dashIcon = (UIImageView *)[cell viewWithTag:203];
   
    
    dashBackground.layer.cornerRadius=15;
    dashName.text=[menuList objectAtIndex:indexPath.row];
    [dashIcon setImage:[UIImage imageNamed:[menuList objectAtIndex:indexPath.row]]];
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = dashBackground.bounds;
    gradient.colors = [NSArray arrayWithObjects:(id)[[UIColor grayColor] CGColor], (id)[[UIColor blackColor] CGColor], nil];
    [dashBackground.layer insertSublayer:gradient atIndex:0];
  
//    CALayer *upperBorder = [CALayer layer];
//    upperBorder.backgroundColor = [[UIColor greenColor] CGColor];
//    upperBorder.frame = CGRectMake(0, 0, CGRectGetWidth(cell.frame), 1.0f);
//    [cell.layer addSublayer:upperBorder];
    
   

//        cell.layer.borderWidth=0.5f;
//    cell.layer.borderColor=[UIColor darkGrayColor].CGColor;
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    float cellWidth = screenWidth /3.16; //Replace the divisor with the column count requirement. Make sure to have it in float.
    CGSize size = CGSizeMake(cellWidth, cellWidth);
    
    return size;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    switch (indexPath.row) {
        case 0:
            [self performSegueWithIdentifier:@"Dashboard" sender:self];
            break;
        case 1:
            [self performSegueWithIdentifier:@"Majid" sender:self];
            break;
            case 2:
            [self performSegueWithIdentifier:@"Dashboard" sender:self];

            break;
        case 3:
            [self performSegueWithIdentifier:@"AsazController" sender:self];
            break;
        default:

            break;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
{
    return YES;
}
@end
