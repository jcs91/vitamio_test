//
//  LiveTVController.h
//  Vitamio
//
//  Created by Joyal Serrao on 17/01/17.
//  Copyright © 2017 Joyal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LiveTVController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *listOfTvTable;
@property (weak, nonatomic) IBOutlet UIView *tvDisplayView;
@property (weak, nonatomic) IBOutlet UIButton *play_pauseBtn;
@property (weak, nonatomic) IBOutlet UIButton *pause_playBtn;
@property (weak, nonatomic) IBOutlet UIButton *expand;

@end
