//
//  Dashboard.h
//  Vitamio
//
//  Created by Joyal Serrao on 16/01/17.
//  Copyright © 2017 Joyal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Dashboard : UIViewController
@property (weak, nonatomic) IBOutlet UICollectionView *dashboardCollection;

@end
