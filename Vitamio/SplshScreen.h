//
//  SplshScreen.h
//  Vitamio
//
//  Created by Joyal Serrao on 16/01/17.
//  Copyright © 2017 Joyal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SplshScreen : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *splshImageView;

@end
