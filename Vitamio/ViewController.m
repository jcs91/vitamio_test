//
//  ViewController.m
//  Vitamio
//
//  Created by Joyal Serrao on 15/01/17.
//  Copyright © 2017 Joyal. All rights reserved.
//

#import "ViewController.h"
#import "Vitamio.h"

@interface ViewController ()<VMediaPlayerDelegate>
{
    VMediaPlayer       *mMPayer;
//    long               mDuration;
//    long               mCurPostion;
//    NSTimer            *mSyncSeekTimer;
}
@property (weak, nonatomic) IBOutlet UIView *playerView;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    }


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark

- (void)mediaPlayer:(VMediaPlayer *)player didPrepared:(id)arg
{
    [player start];
}
// This protocol method will be called when playback complete, so we can
// do something here, e.g. reset player.
- (void)mediaPlayer:(VMediaPlayer *)player playbackComplete:(id)arg
{
    [player reset];
}
// If an error occur, this protocol method will be triggered.
- (void)mediaPlayer:(VMediaPlayer *)player error:(id)arg
{
    NSLog(@"NAL 1RRE &&&& VMediaPlayer Error: %@", arg);
}
- (IBAction)play_pause:(id)sender {
    NSString *videoUrl = @"rtmp://54.255.176.172/live/newsnation_720p";
    
    mMPayer = [VMediaPlayer sharedInstance];
    [mMPayer setupPlayerWithCarrierView:self.playerView
                           withDelegate:self];
    
    [mMPayer setDataSource:[NSURL URLWithString:videoUrl] header:nil];
    [mMPayer prepareAsync];

}


@end
