//
//  MasjidController.m
//  Vitamio
//
//  Created by Joyal Serrao on 17/01/17.
//  Copyright © 2017 Joyal. All rights reserved.
//

#import "MasjidController.h"

@interface MasjidController ()
{
    NSArray *menuList;
}
@end

@implementation MasjidController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
      menuList=[[NSArray alloc]initWithObjects:@"BBC ",@"DW TV",@"XKK",@"Big New Channel",@"XYZ news",@"Channel 1",@"BBC ",@"DW TV",@"XKK",@"Big New Channel",@"XYZ news",@"Channel 1",nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark tableView

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return menuList.count;
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"OnDemandCell";
    
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell==nil) {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
        
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    UILabel *no=(UILabel *)[cell viewWithTag:401];
    UIImageView *icon=(UIImageView *)[cell viewWithTag:403];
    UIView *trasp=(UIView *)[cell viewWithTag:402];
    UILabel *name=(UILabel *)[cell viewWithTag:404];
    
    trasp.backgroundColor = [UIColor colorWithWhite:1 alpha:0.5];
    
    no.text=[NSString stringWithFormat:@"%ld",(long)indexPath.row];
    name.text=[NSString stringWithFormat:@"%@",[menuList objectAtIndex:indexPath.row]];
    [icon setImage:[UIImage imageNamed:@"bbc"]];
    
    return cell;
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


#pragma mark navigation
- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
{
    return YES;
}

@end
