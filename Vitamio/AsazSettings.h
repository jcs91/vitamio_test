//
//  AsazSettings.h
//  Vitamio
//
//  Created by Joyal Serrao on 17/01/17.
//  Copyright © 2017 Joyal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AsazSettings : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *asrTableView;

@end
