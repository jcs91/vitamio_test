//
//  TVDashBoared.m
//  Vitamio
//
//  Created by Joyal Serrao on 17/01/17.
//  Copyright © 2017 Joyal. All rights reserved.
//

#import "TVDashBoared.h"
#import "SlideNavigationController.h"
@interface TVDashBoared ()<SlideNavigationControllerDelegate>
@property (nonatomic) CAPSPageMenu *pagemenu;

@end

@implementation TVDashBoared

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title=@"Home";
    
    NSMutableArray *controllerArray = [NSMutableArray array];
    UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    UIViewController *cont1=[storyboard instantiateViewControllerWithIdentifier:@"LiveTVController"];
    cont1.title=@"TVSHOWS";
    UIViewController *cont2=[storyboard instantiateViewControllerWithIdentifier:@"LiveTVController"];
    cont2.title=@"MOVIES";

    UIViewController *cont3=[storyboard instantiateViewControllerWithIdentifier:@"LiveTVController"];
    cont3.title=@"KIDS";
    UIViewController *cont4=[storyboard instantiateViewControllerWithIdentifier:@"LiveTVController"];
    cont4.title=@"DRAMA";
    UIViewController *cont5=[storyboard instantiateViewControllerWithIdentifier:@"LiveTVController"];
    cont5.title=@"NETWORK";
    [controllerArray addObject:cont1];
    [controllerArray addObject:cont2];
    [controllerArray addObject:cont3];
    [controllerArray addObject:cont4];
    [controllerArray addObject:cont5];


    NSLog(@"%lu",(unsigned long)controllerArray.count);
    
    NSDictionary *parameters = @{
                                 CAPSPageMenuOptionScrollMenuBackgroundColor: [UIColor darkGrayColor],
                                 CAPSPageMenuOptionViewBackgroundColor: [UIColor clearColor],
                                 CAPSPageMenuOptionSelectionIndicatorColor:[UIColor colorWithRed:0 green:0 blue:1 alpha:1],
                                 CAPSPageMenuOptionBottomMenuHairlineColor: [UIColor colorWithRed:70.0/255.0 green:70.0/255.0 blue:70.0/255.0 alpha:1.0],
                                 CAPSPageMenuOptionMenuItemFont: [UIFont fontWithName:@"HelveticaNeue" size:13.0],
                                 CAPSPageMenuOptionMenuHeight: @(40.0),
                                 CAPSPageMenuOptionMenuItemWidth: @(80),
                                 CAPSPageMenuOptionCenterMenuItems: @(YES),
                                 CAPSPageMenuOptionMenuMargin:@(0)
                                 
                                 };
    
    CGFloat Nvheight=self.navigationController.navigationBar.frame.size.height+[UIApplication sharedApplication].statusBarFrame.size.height;
    
    _pagemenu.delegate = self;

    _pagemenu = [[CAPSPageMenu alloc] initWithViewControllers:controllerArray frame:CGRectMake(0.0, Nvheight, self.view.frame.size.width, self.view.frame.size.height-Nvheight) options:parameters];
    
    
    [self.view addSubview:_pagemenu.view];


}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)slideNavigationControllerShouldDisplayLeftMenu{
    return YES;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
