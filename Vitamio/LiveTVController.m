//
//  LiveTVController.m
//  Vitamio
//
//  Created by Joyal Serrao on 17/01/17.
//  Copyright © 2017 Joyal. All rights reserved.
//

#import "LiveTVController.h"
#import "Vitamio.h"
#define kBackviewDefaultRect		CGRectMake(20, 47, 280, 180)

@interface LiveTVController ()<VMediaPlayerDelegate>
{
    VMediaPlayer       *mMPayer;
    NSArray *menuList;
}
@end

@implementation LiveTVController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    menuList=[[NSArray alloc]initWithObjects:@"Live TV",@"Masjid",@"On Demand",@"Azan",@"Phone Pluse",@"Money Transfer",@"Live TV",@"Masjid",@"On Demand",@"Azan",nil];


    if (!mMPayer) {
        mMPayer = [VMediaPlayer sharedInstance];
        //[mMPayer setupPlayerWithCarrierView:self.tvDisplayView withDelegate:self];
        [self setupObservers];
    }


}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return menuList.count;
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"LiveTVCell";
    
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell==nil) {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
        
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
     UILabel *no=(UILabel *)[cell viewWithTag:301];
    UIImageView *icon=(UIImageView *)[cell viewWithTag:303];
    UIView *trasp=(UIView *)[cell viewWithTag:302];
    UILabel *name=(UILabel *)[cell viewWithTag:304];
    
    trasp.backgroundColor = [UIColor colorWithWhite:1 alpha:0.5];

    no.text=[NSString stringWithFormat:@"%ld",(long)indexPath.row];
    name.text=[NSString stringWithFormat:@"%@",[menuList objectAtIndex:indexPath.row]];
   [icon setImage:[UIImage imageNamed:@"bbc"]];
    
    return cell;
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


#pragma mark Vitamio Deleagte

- (void)mediaPlayer:(VMediaPlayer *)player didPrepared:(id)arg
{
    [player start];
}
// This protocol method will be called when playback complete, so we can
// do something here, e.g. reset player.
- (void)mediaPlayer:(VMediaPlayer *)player playbackComplete:(id)arg
{
    [player reset];
}
// If an error occur, this protocol method will be triggered.
- (void)mediaPlayer:(VMediaPlayer *)player error:(id)arg
{
    NSLog(@"NAL 1RRE &&&& VMediaPlayer Error: %@", arg);
}
- (IBAction)play_pause:(id)sender {
    
    NSString *videoUrl = @"rtmp://54.255.176.172/live/newsnation_720p";
    
    mMPayer = [VMediaPlayer sharedInstance];
    [mMPayer setupPlayerWithCarrierView:self.tvDisplayView
                           withDelegate:self];
    
    [mMPayer setDataSource:[NSURL URLWithString:videoUrl] header:nil];
    [mMPayer prepareAsync];
    
}
- (IBAction)expandBtn:(id)sender {
    static int bigView = 0;
    
    [UIView animateWithDuration:0.3 animations:^{
        if (bigView) {
           // self.tvDisplayView.frame =CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height/2);
                       bigView = 0;
    
    self.tvDisplayView.translatesAutoresizingMaskIntoConstraints = NO;

  

        } else {
            self.tvDisplayView.frame = self.view.bounds;
            bigView = 1;
        }
        NSLog(@"NAL 1NBV &&&& backview.frame=%@", NSStringFromCGRect(self.tvDisplayView.frame));
    }];

}


- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskAll;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)to duration:(NSTimeInterval)duration
{
    if (UIInterfaceOrientationIsLandscape(to)) {
        self.tvDisplayView.frame = self.view.bounds;
    } else {
        self.tvDisplayView.frame = kBackviewDefaultRect;
    }
   // NSLog(@"NAL 1HUI &&&&&&&&& frame=%@", NSStringFromCGRect(self.carrier.frame));
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
  //  NSLog(@"NAL 2HUI &&&&&&&&& frame=%@", NSStringFromCGRect(self.carrier.frame));
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [self unSetupObservers];

    [self resignFirstResponder];
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
  //  [mMPayer unSetupPlayer];
    //[mMPayer clearCache];
    
}





#pragma mark - Respond to the Remote Control Events

- (BOOL)canBecomeFirstResponder
{
    return YES;
}

- (void)remoteControlReceivedWithEvent:(UIEvent *)event
{
    switch (event.subtype) {
        case UIEventSubtypeRemoteControlTogglePlayPause:
            if ([mMPayer isPlaying]) {
                [mMPayer pause];
            } else {
                [mMPayer start];
            }
            break;
        case UIEventSubtypeRemoteControlPlay:
            [mMPayer start];
            break;
        case UIEventSubtypeRemoteControlPause:
            [mMPayer pause];
            break;
        case UIEventSubtypeRemoteControlPreviousTrack:
         //   [self prevButtonAction:nil];
            break;
        case UIEventSubtypeRemoteControlNextTrack:
          //  [self nextButtonAction:nil];
            break;
        default:
            break;
    }
}

- (void)applicationDidEnterForeground:(NSNotification *)notification
{
    [mMPayer setVideoShown:YES];
    if (![mMPayer isPlaying]) {
        [mMPayer start];
       // [self.startPause setTitle:@"Pause" forState:UIControlStateNormal];
    }
}

- (void)applicationDidEnterBackground:(NSNotification *)notification
{
    if ([mMPayer isPlaying]) {
        [mMPayer pause];
        [mMPayer setVideoShown:NO];
    }
}

- (void)unSetupObservers
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}



- (void)setupObservers
{
    NSNotificationCenter *def = [NSNotificationCenter defaultCenter];
    [def addObserver:self
            selector:@selector(applicationDidEnterForeground:)
                name:UIApplicationDidBecomeActiveNotification
              object:[UIApplication sharedApplication]];
    [def addObserver:self
            selector:@selector(applicationDidEnterBackground:)
                name:UIApplicationWillResignActiveNotification
              object:[UIApplication sharedApplication]];
}


@end
