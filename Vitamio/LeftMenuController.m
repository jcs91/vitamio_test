//
//  LeftMenuController.m
//  Vitamio
//
//  Created by Joyal Serrao on 16/01/17.
//  Copyright © 2017 Joyal. All rights reserved.
//

#import "LeftMenuController.h"
#import "SlideNavigationController.h"

@interface LeftMenuController ()<UITableViewDelegate,UITableViewDataSource>
{
   NSArray *menuList;
}
@end

@implementation LeftMenuController


- (id)initWithCoder:(NSCoder *)aDecoder
{
    self.slideOutAnimationEnabled = NO;
    
    return [super initWithCoder:aDecoder];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    menuList=[[NSArray alloc]initWithObjects:@"Live TV",@"Masjid",@"On Demand",@"Azan",@"Phone Pluse",@"Money Transfer",nil];
    

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark -Table methods

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return menuList.count;
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"menuCell";
    
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell==nil) {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
        
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
     UIImageView *icon=(UIImageView *)[cell viewWithTag:101];
    UILabel *name=(UILabel *)[cell viewWithTag:102];
    
    name.text=[NSString stringWithFormat:@"%@",[menuList objectAtIndex:indexPath.row]];
    [icon setImage:[UIImage imageNamed:[menuList objectAtIndex:indexPath.row]]];

    return cell;
    
}

-(UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    static NSString *CellIdentifier = @"menuHeader";
    UITableViewCell *headerView = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
   
   
    
    if (headerView == nil){
        [NSException raise:@"headerView == nil.." format:@"No cells with matching CellIdentifier loaded from your storyboard"];
    }
    return headerView;
}




- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    
    UIViewController *vc;
    
    switch (indexPath.row)
    {
        case 0:
            vc = [mainStoryboard instantiateViewControllerWithIdentifier: @"TVDashBoared"];
            [vc setTitle:@"Home"];

            break;
        case 1:
            vc = [mainStoryboard instantiateViewControllerWithIdentifier: @"MasjidController"];
            [vc setTitle:@"Home"];

            //ServicesDashboard
            break;
        case 2:
            
            vc=[mainStoryboard instantiateViewControllerWithIdentifier:@"TVDashBoared"];
            [vc setTitle:@"Home"];
            break;
            
            
        case 3:
            vc = [mainStoryboard instantiateViewControllerWithIdentifier: @"AsazController"];
            [vc setTitle:@"Home"];
            
            //            [self.menuList deselectRowAtIndexPath:[self.menuList indexPathForSelectedRow] animated:YES];
            //            [[SlideNavigationController sharedInstance] popToRootViewControllerAnimated:YES];
            
            break;
            
        case 4:
            
          
            break;
            
        case 5:
            
            break;
        case 6:
            
            
            break;
    }
    
    
    [[SlideNavigationController sharedInstance] popToRootAndSwitchToViewController:vc
                                                             withSlideOutAnimation:self.slideOutAnimationEnabled
                                                                     andCompletion:^{
                                                                         
                                                                         
                                                                     }];
    
    
}
- (IBAction)logoutBtn:(id)sender {
    //  [[SlideNavigationController sharedInstance] popToRootViewControllerAnimated:YES];
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    UIViewController *vc=[mainStoryboard instantiateViewControllerWithIdentifier:@"SplashScreenController"];
    
    
    //SplashScreenController
    //LoginController1
    [[SlideNavigationController sharedInstance]popToRootAndSwitchToViewController:vc withSlideOutAnimation:NO andCompletion:nil];
    
    //   [self performSegueWithIdentifier:@"LogOut" sender:sender];
    
    
    
    
    
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
